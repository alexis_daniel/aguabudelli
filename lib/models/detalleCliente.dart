import 'dart:convert';

DetalleCliente detalleClienteFromJson(String str) => DetalleCliente.fromJson(json.decode(str));

String detalleClienteToJson(DetalleCliente data) => json.encode(data.toJson());

class DetalleCliente {
  String? status;
  String? code;
  String? idLr;
  String? total;
  DatosDelCliente? datosDelCliente;
  DetalleDelCliente? detalleDelCliente;
  List<Pedido>? pedido;

  DetalleCliente({
    this.status,
    this.code,
    this.idLr,
    this.total,
    this.datosDelCliente,
    this.detalleDelCliente,
    this.pedido,
  });

  factory DetalleCliente.fromJson(Map<String, dynamic> json) => DetalleCliente(
    status: json["status"],
    code: json["code"],
    idLr: json["id_LR"],
    total: json["total"],
    datosDelCliente: json["DatosDelCliente"] == null ? null : DatosDelCliente.fromJson(json["DatosDelCliente"]),
    detalleDelCliente: json["DetalleDelCliente"] == null ? null : DetalleDelCliente.fromJson(json["DetalleDelCliente"]),
    pedido: json["Pedido"] == null ? [] : List<Pedido>.from(json["Pedido"]!.map((x) => Pedido.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "code": code,
    "id_LR": idLr,
    "total": total,
    "DatosDelCliente": datosDelCliente?.toJson(),
    "DetalleDelCliente": detalleDelCliente?.toJson(),
    "Pedido": pedido == null ? [] : List<dynamic>.from(pedido!.map((x) => x.toJson())),
  };
}

class DatosDelCliente {
  String? nombre;
  String? telfono;

  DatosDelCliente({
    this.nombre,
    this.telfono,
  });

  factory DatosDelCliente.fromJson(Map<String, dynamic> json) => DatosDelCliente(
    nombre: json["Nombre"],
    telfono: json["Teléfono"],
  );

  Map<String, dynamic> toJson() => {
    "Nombre": nombre,
    "Teléfono": telfono,
  };
}

class DetalleDelCliente {
  List<String>? totalGarrafonesEnEfectivo;
  List<String>? totalGarrafonesEnVales;
  List<String>? totalGarrafonesACrdito;
  List<String>? totalGeneral;
  List<String>? stockDeGarrafones;

  DetalleDelCliente({
    this.totalGarrafonesEnEfectivo,
    this.totalGarrafonesEnVales,
    this.totalGarrafonesACrdito,
    this.totalGeneral,
    this.stockDeGarrafones,
  });

  factory DetalleDelCliente.fromJson(Map<String, dynamic> json) => DetalleDelCliente(
    totalGarrafonesEnEfectivo: json["Total garrafones en efectivo "] == null ? [] : List<String>.from(json["Total garrafones en efectivo "]!.map((x) => x)),
    totalGarrafonesEnVales: json["Total garrafones en vales "] == null ? [] : List<String>.from(json["Total garrafones en vales "]!.map((x) => x)),
    totalGarrafonesACrdito: json["Total garrafones a crédito"] == null ? [] : List<String>.from(json["Total garrafones a crédito"]!.map((x) => x)),
    totalGeneral: json["Total general"] == null ? [] : List<String>.from(json["Total general"]!.map((x) => x)),
    stockDeGarrafones: json["Stock de garrafones"] == null ? [] : List<String>.from(json["Stock de garrafones"]!.map((x) => x)),
  );

  Map<String, dynamic> toJson() => {
    "Total garrafones en efectivo ": totalGarrafonesEnEfectivo == null ? [] : List<dynamic>.from(totalGarrafonesEnEfectivo!.map((x) => x)),
    "Total garrafones en vales ": totalGarrafonesEnVales == null ? [] : List<dynamic>.from(totalGarrafonesEnVales!.map((x) => x)),
    "Total garrafones a crédito": totalGarrafonesACrdito == null ? [] : List<dynamic>.from(totalGarrafonesACrdito!.map((x) => x)),
    "Total general": totalGeneral == null ? [] : List<dynamic>.from(totalGeneral!.map((x) => x)),
    "Stock de garrafones": stockDeGarrafones == null ? [] : List<dynamic>.from(stockDeGarrafones!.map((x) => x)),
  };
}

class Pedido {
  String? idProducto;
  String? nombreDelProducto;
  String? precio;
  String? idDominio;

  Pedido({
    this.idProducto,
    this.nombreDelProducto,
    this.precio,
    this.idDominio,
  });

  factory Pedido.fromJson(Map<String, dynamic> json) => Pedido(
    idProducto: json["id_producto"],
    nombreDelProducto: json["NombreDelProducto"],
    precio: json["Precio"],
    idDominio: json["id_dominio"],
  );

  Map<String, dynamic> toJson() => {
    "id_producto": idProducto,
    "NombreDelProducto": nombreDelProducto,
    "Precio": precio,
    "id_dominio": idDominio,
  };
}
