import 'dart:convert';

Totales totalesFromJson(String str) => Totales.fromJson(json.decode(str));

String totalesToJson(Totales data) => json.encode(data.toJson());

class Totales {
  String? status;
  int? code;
  Data? data;

  Totales({
    this.status,
    this.code,
    this.data,
  });

  factory Totales.fromJson(Map<String, dynamic> json) => Totales(
    status: json["status"],
    code: json["code"],
    data: json["data"] == null ? null : Data.fromJson(json["data"]),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "code": code,
    "data": data?.toJson(),
  };
  static Totales totales = Totales();
}

class Data {
  List<String>? totalGarrafonesEnEfectivo;
  List<String>? totalGarrafonesEnVales;
  List<String>? totalGarrafonesACrdito;
  List<String>? totalGeneral;
  List<String>? stockDeGarrafones;

  Data({
    this.totalGarrafonesEnEfectivo,
    this.totalGarrafonesEnVales,
    this.totalGarrafonesACrdito,
    this.totalGeneral,
    this.stockDeGarrafones,
  });

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    totalGarrafonesEnEfectivo: json["Total garrafones en efectivo "] == null ? [] : List<String>.from(json["Total garrafones en efectivo "]!.map((x) => x)),
    totalGarrafonesEnVales: json["Total garrafones en vales "] == null ? [] : List<String>.from(json["Total garrafones en vales "]!.map((x) => x)),
    totalGarrafonesACrdito: json["Total garrafones a crédito"] == null ? [] : List<String>.from(json["Total garrafones a crédito"]!.map((x) => x)),
    totalGeneral: json["Total general"] == null ? [] : List<String>.from(json["Total general"]!.map((x) => x)),
    stockDeGarrafones: json["Stock de garrafones"] == null ? [] : List<String>.from(json["Stock de garrafones"]!.map((x) => x)),
  );

  Map<String, dynamic> toJson() => {
    "Total garrafones en efectivo ": totalGarrafonesEnEfectivo == null ? [] : List<dynamic>.from(totalGarrafonesEnEfectivo!.map((x) => x)),
    "Total garrafones en vales ": totalGarrafonesEnVales == null ? [] : List<dynamic>.from(totalGarrafonesEnVales!.map((x) => x)),
    "Total garrafones a crédito": totalGarrafonesACrdito == null ? [] : List<dynamic>.from(totalGarrafonesACrdito!.map((x) => x)),
    "Total general": totalGeneral == null ? [] : List<dynamic>.from(totalGeneral!.map((x) => x)),
    "Stock de garrafones": stockDeGarrafones == null ? [] : List<dynamic>.from(stockDeGarrafones!.map((x) => x)),
  };
}
