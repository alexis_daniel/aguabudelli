import 'dart:convert';

import '../data/login.dart';

Clientes clientesFromJson(String str) => Clientes.fromJson(json.decode(str));

String clientesToJson(Clientes data) => json.encode(data.toJson());

class Clientes {
  String? status;
  int? code;
  String? total;
  Map<String, Datum>? data;

  Clientes({
    this.status,
    this.code,
    this.total,
    this.data,
  });

  factory Clientes.fromJson(Map<String, dynamic> json) => Clientes(
    status: json["status"],
    code: json["code"],
    total: json["total"],
    data: Map.from(json["data"]!).map((k, v) => MapEntry<String, Datum>(k, Datum.fromJson(v))),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "code": code,
    "total": total,
    "data": Map.from(data!).map((k, v) => MapEntry<String, dynamic>(k, v.toJson())),
  };

  static Clientes clientes = Clientes();
  getClientes(String admin, String crm, String perfil, String palabra)async{
    Clientes temp = Clientes();
    try{
      dynamic json = await LoginDataRequest().getClientes(admin, crm, perfil, palabra);
      temp = Clientes.fromJson(json);
      clientes = temp;
    }catch(_){}
  }
}

class Datum {
  String? idLr;
  String? nombre;
  String? nombreDelNegocio;
  String? imagen;
  String? alias;
  String? lat;
  String? lon;
  String? direccion;

  Datum({
    this.idLr,
    this.nombre,
    this.nombreDelNegocio,
    this.imagen,
    this.alias,
    this.lat,
    this.lon,
    this.direccion,
  });

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
    idLr: json["id_LR"],
    nombre: json["Nombre"],
    nombreDelNegocio: json["NombreDelNegocio"],
    imagen: json["Imagen"],
    alias: json["Alias"],
    lat: json["Lat"],
    lon: json["Lon"],
    direccion: json["Direccion"],
  );

  Map<String, dynamic> toJson() => {
    "id_LR": idLr,
    "Nombre": nombre,
    "NombreDelNegocio": nombreDelNegocio,
    "Imagen": imagen,
    "Alias": alias,
    "Lat": lat,
    "Lon": lon,
    "Direccion": direccion,
  };
}
