// To parse this JSON data, do
//
//     final direccion = direccionFromJson(jsonString);

import 'dart:convert';

Direccion direccionFromJson(String str) => Direccion.fromJson(json.decode(str));

String direccionToJson(Direccion data) => json.encode(data.toJson());

class Direccion {
  String? status;
  int? code;
  String? total;
  Data? data;

  Direccion({
    this.status,
    this.code,
    this.total,
    this.data,
  });

  factory Direccion.fromJson(Map<String, dynamic> json) => Direccion(
    status: json["status"],
    code: json["code"],
    total: json["total"],
    data: json["data"] == null ? null : Data.fromJson(json["data"]),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "code": code,
    "total": total,
    "data": data?.toJson(),
  };
}

class Data {
  String? titulo;
  String? subTitulo;
  String? header;
  String? footer;

  Data({
    this.titulo,
    this.subTitulo,
    this.header,
    this.footer,
  });

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    titulo: json["Titulo"],
    subTitulo: json["SubTitulo"],
    header: json["Header"],
    footer: json["Footer"],
  );

  Map<String, dynamic> toJson() => {
    "Titulo": titulo,
    "SubTitulo": subTitulo,
    "Header": header,
    "Footer": footer,
  };
}
