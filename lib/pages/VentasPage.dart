import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:ippsonline/models/detalleVenta.dart';
import 'package:ippsonline/models/totales.dart';
import 'package:ippsonline/models/ventaModel.dart';
import 'package:ippsonline/pages/printData.dart';
import 'package:ippsonline/pages/printData3.dart';
import 'package:ndialog/ndialog.dart';
import 'package:provider/provider.dart';

import '../data/login.dart';
import '../models/direccion.dart';
import '../models/loginModel.dart';
import '../models/pedidosModel.dart';
import '../providers/session.dart';

class VentasPage extends StatefulWidget {
  const VentasPage({super.key});

  @override
  State<VentasPage> createState() => _VentasPageState();
}

class _VentasPageState extends State<VentasPage> {

  showDetails(int index)async{
    var json = await LoginDataRequest().getDetalleVenta(
        LoginData.user.idAdmin.toString(),
        LoginData.user.idCrm.toString(),
        Venta.ventas[index].idEntrega.toString(),
        Venta.ventas[index].idPv.toString(),
        Venta.ventas[index].idLr.toString()
        );
    List<DetalleVenta> temp = [];
    try{
      for (var i = 0; i < json["data"].length; i++) {
        temp.add(DetalleVenta.fromJson(json["data"][i]));
      }
    }catch(_){}
     showDetalle(temp, index);
  }


  void showDetalle(List<DetalleVenta> detalleVenta, int index){
    showDialog(context: context, builder: (context){
      return AlertDialog(
        content: SingleChildScrollView(
          child: Column(
            children: detalleVenta.map((e) {
              return ListTile(
                title:  Text(e.nombreDelProducto.toString(), style: TextStyle(fontWeight: FontWeight.w700),),

                subtitle: Align(
                  alignment: Alignment.centerLeft,
                  child: Padding(
                    padding: const EdgeInsets.only(left: 10.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Column(
                          mainAxisSize: MainAxisSize.min,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                                   Text(e.cantidad.toString() + " X " + "\$"+ e.precioVendido.toString(),style: TextStyle(fontWeight: FontWeight.w300),),
                            Text(e.metodoDePago.toString(),style: TextStyle(fontWeight: FontWeight.w300),),
                            Text(e.tipoDePago.toString(),style: TextStyle(fontWeight: FontWeight.w300),),
                            Text( DateFormat("yyyy-MM-dd").format(e.fechaHora!),style: TextStyle(fontWeight: FontWeight.w300),)
                          ],
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text("\$" + e.total.toString(), style: TextStyle(color: Colors.grey ),),
                            IconButton(onPressed: ()async{
                              var json = await LoginDataRequest().getDireccion(LoginData.user.idAdmin.toString(), LoginData.user.idCrm.toString(), Venta.ventas[index].idPv.toString());
                              Navigator.of(context).pop();
                              Navigator.push(context, MaterialPageRoute(builder: (context) => PrintData(detalleVenta, index, Direccion.fromJson(json))));
                            }, icon: const Icon(Icons.print))
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              );
            }).toList(),
          ),
        ),
      );
    });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        iconTheme: const IconThemeData(color: Colors.white),
        title: const Text("Ventas", style: TextStyle(color: Colors.white),),
        flexibleSpace: Container(
          decoration: const BoxDecoration(
              image: DecorationImage(
                image: AssetImage("assets/logo/background.jpg"),
                fit: BoxFit.cover,
              ),
              borderRadius: BorderRadius.only(
                  bottomRight: Radius.circular(12),
                  bottomLeft: Radius.circular(12))),
        ),
      ),
      body: SingleChildScrollView(
        physics: ClampingScrollPhysics(),
        child: Column(
          children: [
            ListView.builder(
              physics: ClampingScrollPhysics(),
              shrinkWrap: true,
              itemBuilder: (context, int index){
              return ListTile(
                onTap: (){
                  showDetails(index);
                },
                title: Text(Venta.ventas[index].nombre.toString() + (Venta.ventas[index].nombreDelNegocio.toString().isNotEmpty || Venta.ventas[index].alias.toString().isNotEmpty ? " (" : "") + Venta.ventas[index].nombreDelNegocio.toString() + ((Venta.ventas[index].alias.toString().isNotEmpty ? ("-> " + Venta.ventas[index].alias.toString()) : "") + ((Venta.ventas[index].nombreDelNegocio.toString().isNotEmpty || Venta.ventas[index].alias.toString().isNotEmpty) ? ")" : "")),style: const TextStyle(fontWeight: FontWeight.w700),),
                trailing:  Text(
                  "\$" +
                      (Venta.ventas[index].total.toString()),
                  style: const TextStyle(color: Colors.grey),
                ),
              );
            }, itemCount: Venta.ventas.length,),
             const SizedBox(height: 20,),
            Table(
              columnWidths: <int, TableColumnWidth>{
                0: FlexColumnWidth(3),
                1: FlexColumnWidth(1),
                2: FlexColumnWidth(1),
              },
              defaultVerticalAlignment: TableCellVerticalAlignment.middle,
              children: [
                TableRow(
                  children: [
                    Text("Total garrafones en efectivo",style: TextStyle(fontWeight: FontWeight.w600),textAlign: TextAlign.right,),
                    Text(Totales.totales.data!.totalGarrafonesEnEfectivo!.elementAt(0).toString(),textAlign: TextAlign.center),
                    Text(Totales.totales.data!.totalGarrafonesEnEfectivo!.elementAt(1).toString(),textAlign: TextAlign.center),
                  ]
                ),
                TableRow(
                    children: [
                      Text("Total garrafones en Vales",style: TextStyle(fontWeight: FontWeight.w600),textAlign: TextAlign.right,),
                      Text(Totales.totales.data!.totalGarrafonesEnVales!.elementAt(0).toString(),textAlign: TextAlign.center),
                      Text(Totales.totales.data!.totalGarrafonesEnVales!.elementAt(1).toString(),textAlign: TextAlign.center)
                    ]
                ),
                TableRow(
                    children: [
                      Text("Total garrafones a Crédito",style: TextStyle(fontWeight: FontWeight.w600),textAlign: TextAlign.right,),
                      Text(Totales.totales.data!.totalGarrafonesACrdito!.elementAt(0).toString(),textAlign: TextAlign.center),
                      Text(Totales.totales.data!.totalGarrafonesACrdito!.elementAt(1).toString(),textAlign: TextAlign.center)
                    ]
                ),
                TableRow(
                    children: [
                      Text("Total General",style: TextStyle(fontWeight: FontWeight.w600),textAlign: TextAlign.right,),
                      Text(Totales.totales.data!.totalGeneral!.elementAt(0).toString(),textAlign: TextAlign.center),
                      Text(Totales.totales.data!.totalGeneral!.elementAt(1).toString(),textAlign: TextAlign.center),
                    ]
                ),
                TableRow(
                    children: [
                      Text("Stock de garrafones",style: TextStyle(fontWeight: FontWeight.w600),textAlign: TextAlign.right,),
                      Text(Totales.totales.data!.stockDeGarrafones!.elementAt(0).toString(),textAlign: TextAlign.center),
                      Text(Totales.totales.data!.stockDeGarrafones!.elementAt(1).toString(),textAlign: TextAlign.center,),
                    ]
                ),
              ]
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton.extended(
        label: const Text("Imprimir ventas"),
        icon: const Icon(Icons.print),
        onPressed: ()async{
          CustomProgressDialog progressDialog = CustomProgressDialog(
            context,
            blur: 10,
          );
          //Set loading with red circular progress indicator
          progressDialog.setLoadingWidget(const CircularProgressIndicator(
              valueColor: AlwaysStoppedAnimation(Colors.red)));
          progressDialog.show();
         List<List<DetalleVenta>> temp2 = [];
          for (var i in Venta.ventas){
            var json = await LoginDataRequest().getDetalleVenta(
                LoginData.user.idAdmin.toString(),
                LoginData.user.idCrm.toString(),
                i.idEntrega.toString(),
                i.idPv.toString(),
                i.idLr.toString()
            );
            List<DetalleVenta> temp = [];
            try{
              for (var i = 0; i < json["data"].length; i++) {
                temp.add(DetalleVenta.fromJson(json["data"][i]));
              }
            }catch(_){}
            temp2.add(temp);
          }

          var json = await LoginDataRequest().getDireccion(LoginData.user.idAdmin.toString(), LoginData.user.idCrm.toString(), Venta.ventas.first.idPv.toString());
          progressDialog.dismiss();
          Navigator.push(context, MaterialPageRoute(builder: (context) => PrintData3(temp2, "", Direccion.fromJson(json))));
        }
      ),
    );
  }
}
