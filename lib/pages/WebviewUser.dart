import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:webview_flutter/webview_flutter.dart';

import 'LoginPage.dart';

class WebviewUser extends StatelessWidget {
  String correo = "";
  String pass = "";
  WebviewUser({required this.correo ,required this.pass,super.key});
  String url = "https://aguabudelli.com/adm/WebViewHtml.php?Correo=demo@purificadora.com&Contrasena=123456";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: [
          IconButton(onPressed: ()async{
            SharedPreferences shared = await SharedPreferences.getInstance();
            await shared.clear();
            Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => LoginPage()));
          }, icon: const Icon(Icons.exit_to_app, color: Colors.black,))
        ],
      ),
      body: WebViewWidget(
     controller: WebViewController()
       ..setJavaScriptMode(JavaScriptMode.unrestricted)
       ..loadRequest(Uri.parse("https://aguabudelli.com/adm/WebViewHtml.php?Correo=$correo&Contrasena=$pass"))
    )
    );
  }
}
