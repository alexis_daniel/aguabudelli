import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:geolocator/geolocator.dart';
import 'package:intl/intl.dart';
import 'package:ippsonline/models/clientes.dart';
import 'package:ippsonline/pages/printData2.dart';
import 'package:ippsonline/pages/ventasBuilder.dart';
import 'package:ndialog/ndialog.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../data/login.dart';
import '../models/detalleCliente.dart';
import '../models/detallePedido.dart';
import '../models/detalleVenta.dart';
import '../models/direccion.dart';
import '../models/loginModel.dart';
import '../models/pedidosModel.dart';
import '../providers/session.dart';
import '../utils/parameters.dart';
import 'DetalleCliente.dart';
import 'HomePage.dart';
import 'MapaPoints.dart';

class ClientesPage extends StatefulWidget {
  const ClientesPage({super.key});

  @override
  State<ClientesPage> createState() => _ClientesPageState();
}

class _ClientesPageState extends State<ClientesPage> {

  void entregarPedido(String lr, String web, String programado,
      String dominio,String name) async {
    Navigator.of(context).pop(false);
    List<DetalleVenta> venta = [];
    int cantidad = 0;
    List<int> elementsIndex = [];
    Provider.of<Session>(context,listen: false).cantidad.forEach((element) {
      cantidad = cantidad +element;
      if(element != 0 ){
        elementsIndex.add(Provider.of<Session>(context,listen: false).cantidad.indexOf(element));
      }
    });
    Map<String, dynamic> productos= {};
    elementsIndex.forEach((element) {
      var a = Detalle.detalle.data!.elementAt(element);
      Map<String, dynamic> data =  {
        a.idProducto.toString() : {
          "Puntos": "",
          "id_producto": a.idProducto.toString(),
          "NombreDelProducto": a.nombreDelProducto,
          "PrecioVendido": Provider.of<Session>(context,listen: false).precios.elementAt(element).toString(),
          "id_EntSal": "",
          "Cantidad": Provider.of<Session>(context,listen: false).cantidad.elementAt(element).toString(),
        }
      };
      venta.add(
          DetalleVenta(
              cantidad: Provider.of<Session>(context,listen: false).cantidad.elementAt(element).toString(),
              subTotal: Provider.of<Session>(context,listen: false).precios.elementAt(element).toString(),
              nombreDelProducto: a.nombreDelProducto,
              precioVendido: Provider.of<Session>(context,listen: false).precios.elementAt(element).toString(),
              total: (double.parse(Provider.of<Session>(context,listen: false).cantidad.elementAt(element).toString()) * double.parse(Provider.of<Session>(context,listen: false).precios.elementAt(element).toString())).toString()
          )
      );
      productos.addAll(data);
    });

    Map entregar = {
      "id_dominio": dominio,
      "id_admin": LoginData.user.idAdmin.toString(),
      "id_CRM": LoginData.user.idCrm.toString(),
      "id_perfil": LoginData.user.idPerfil.toString(),
      "id_LR": lr,
      "id_PWeb": web,
      "id_programado": programado,
      "FolioPersonalizado": "",
      "FechaDeSolicitud": DateFormat('yyyy-MM-dd').format(DateTime.now()),
      "Cantidad":cantidad,
      "SubTotal":geTotal(Detalle.detalle.seleccionados),
      "Total":geTotal(Detalle.detalle.seleccionados),
      "ArrayProductos": productos,
      "TipoDePago": Provider.of<Session>(context,listen: false).payment,
      "MetodoDePago": Provider.of<Session>(context,listen: false).pagos,
      "Referencia": "",
      "Observaciones": "",
    };

    if (await Parameters().checkInternet()) {
      CustomProgressDialog progressDialog = CustomProgressDialog(
        context,
        blur: 10,
      );
      //Set loading with red circular progress indicator
      progressDialog.setLoadingWidget(const CircularProgressIndicator(
          valueColor: AlwaysStoppedAnimation(Colors.red)));
      progressDialog.show();


      int result = await LoginDataRequest().entregarPedido(entregar, context);

      if (result == 2) {
        await Pedidos().getPedidos(
            LoginData.user.idAdmin.toString(),
            LoginData.user.idCrm.toString(),
            LoginData.user.idPerfil.toString());
        progressDialog.dismiss();
        setState(() {});
        showDialog(
            context: context,
            builder: (context) {
              return AlertDialog(
                content: Text("Pedido entregado correctamente"),
                actions: [
                  TextButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: Text("Ok")),
                  TextButton(
                      onPressed: ()async {
                        var json = await LoginDataRequest().getDireccion(LoginData.user.idAdmin.toString(), LoginData.user.idCrm.toString(), Provider.of<Session>(context,listen: false).pv);
                        Navigator.of(context).pop();
                        Navigator.push(context, MaterialPageRoute(builder: (context) => PrintData2(venta,name,Direccion.fromJson(json))));
                      },
                      child: Text("Imprimir")),
                  TextButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                        Navigator.push(context, MaterialPageRoute(builder: (context) => VentasBuilder()));
                      },
                      child: Text("Ir a ventas"))
                ],
              );
            });
      } else {
        progressDialog.dismiss();
        showDialog(
            context: context,
            builder: (context) {
              return AlertDialog(
                content: Text("Ocurrio un error al entregar el pedido"),
              );
            });
      }
    } else {
      SharedPreferences shared = await SharedPreferences.getInstance();
      if (shared.getStringList("pendingEntregar") == null) {
        shared.setStringList('pendingEntregar', [jsonEncode(entregar)]);
      } else {
        List<String> pending =
        shared.getStringList("pendingEntregar") as List<String>;
        pending.add(jsonEncode(entregar));
        shared.setStringList('pendingEntregar', pending);
      }
      showDialog(
          context: context,
          builder: (context) {
            return AlertDialog(
              content: Text(
                  "El pedido se entregará cuando cuente con conexión a internet"),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text("Ok"))
              ],
            );
          });
    }
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        iconTheme: const IconThemeData(color: Colors.white),
        title: const Text("Clientes", style: TextStyle(color: Colors.white),),
        flexibleSpace: Container(
          decoration: const BoxDecoration(
              image: DecorationImage(
                image: AssetImage("assets/logo/background.jpg"),
                fit: BoxFit.cover,
              ),
              borderRadius: BorderRadius.only(
                  bottomRight: Radius.circular(12),
                  bottomLeft: Radius.circular(12))),
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Card(
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
                child: Padding(
                  padding: const EdgeInsets.only(left: 10.0, right: 10),
                  child: TextField(
                    decoration: const InputDecoration(
                      hintText: "Buscar cliente",
                      border: InputBorder.none
                    ),
                    onChanged: (a)async{
                      await Clientes().getClientes(LoginData.user.idAdmin.toString(), LoginData.user.idCrm.toString(),LoginData.user.idPerfil.toString(), a.toString());
                      setState(() {

                      });
                    },
                  ),
                ),
              ),
            ),
            ListView.builder(
              shrinkWrap: true,
              physics: const ClampingScrollPhysics(),
              itemBuilder: (context, int d){
              return Column(
                children: [
                  Stack(
                    children: [
                      ListTile(
                        onTap: (){
                          showDialog(context: context, builder: (context){
                            return AlertDialog(
                              content: SingleChildScrollView(
                                child: Column(
                                  children: [
                                    ListTile(
                                      onTap: ()async{
                                        Navigator.of(context).pop();
                                        var info = await infoPermission();
                                        if(info != null){
                                          Provider.of<Session>(context,listen: false).lat = info.latitude;
                                          Provider.of<Session>(context,listen: false).lng = info.longitude;
                                          Navigator.push(context, MaterialPageRoute(builder: (context) => MapaPoints(
                                              double.parse(Clientes.clientes.data!.values.elementAt(d).lat.toString()),
                                              double.parse(Clientes.clientes.data!.values.elementAt(d).lon.toString())
                                          )));
                                        }else{}
                                      },
                                      leading: const Icon(Icons.location_on),
                                      title: const Text("Ir a ubicación"),
                                    ),
                                    ListTile(
                                      onTap: ()async{
                                        Navigator.of(context).pop();
                                        Provider.of<Session>(context,listen: false).cantidad.clear();
                                        Provider.of<Session>(context,listen: false).precios.clear();
                                        showDetails(d);
                                      },
                                      leading: const Icon(Icons.delivery_dining_rounded),
                                      title: const Text("Entregar pedido"),
                                    ),
                                    ListTile(
                                      onTap: ()async{
                                        var json = await LoginDataRequest().getDetalleCliente(LoginData.user.idAdmin.toString(),LoginData.user.idCrm.toString(), Clientes.clientes.data!.values.elementAt(d).idLr.toString());
                                        DetalleCliente cliente = DetalleCliente.fromJson(json);
                                        Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => DetalleCliente2(cliente)));
                                      },
                                      leading: const Icon(Icons.person),
                                      title: const Text("Detalle del cliente"),
                                    )
                                  ],
                                ),
                              ),
                            );
                          });
                        },
                        leading: CircleAvatar(
                          backgroundImage: NetworkImage(Clientes.clientes.data!.values.elementAt(d).imagen.toString()),
                        ),
                        trailing: SizedBox(width: MediaQuery.of(context).size.width * 0.15,),
                        title: Text(Clientes.clientes.data!.values.elementAt(d).nombre.toString() + (Clientes.clientes.data!.values.elementAt(d).nombreDelNegocio.toString().isNotEmpty || Clientes.clientes.data!.values.elementAt(d).alias.toString().isNotEmpty ? " (" : "") + Clientes.clientes.data!.values.elementAt(d).nombreDelNegocio.toString() + ((Clientes.clientes.data!.values.elementAt(d).alias.toString().isNotEmpty ? ("-> " + Clientes.clientes.data!.values.elementAt(d).alias.toString()) : "") + ((Clientes.clientes.data!.values.elementAt(d).nombreDelNegocio.toString().isNotEmpty || Clientes.clientes.data!.values.elementAt(d).alias.toString().isNotEmpty) ? ")" : "")),style: const TextStyle(fontWeight: FontWeight.w700),),
                      ),
                    ],
                  ),
                  const SizedBox(height:15,),
                ],
              );
            }, itemCount: Clientes.clientes.data!.length,),
          ],
        ),
      ),
    );
  }

  showDetails(int d)async{
    var json = await LoginDataRequest().getDetalle(
        LoginData.user.idAdmin.toString(),
        LoginData.user.idCrm.toString(),
        LoginData.user.idPerfil.toString(),
            "",
        Clientes.clientes.data!.values.elementAt(d).idLr ??
            "",
        "");
    Detalle.detalle = Detalle();
    Detalle.detalle = Detalle.fromJson(json);
    showDetalle(d);
  }

  void cancelPedidos(String lr, String web, String programado) async {
    Navigator.of(context).pop(false);
    Map cancelar = {
      "id_admin": LoginData.user.idAdmin.toString(),
      "id_CRM": LoginData.user.idCrm.toString(),
      "id_perfil": LoginData.user.idAdmin.toString(),
      "id_LR": lr,
      "id_PWeb": web,
      "id_programado": programado
    };
    if (await Parameters().checkInternet()) {
      CustomProgressDialog progressDialog = CustomProgressDialog(
        context,
        blur: 10,
      );
      //Set loading with red circular progress indicator
      progressDialog.setLoadingWidget(const CircularProgressIndicator(
          valueColor: AlwaysStoppedAnimation(Colors.red)));
      progressDialog.show();

      print(cancelar);
      int result = await LoginDataRequest().cancelarPedido(cancelar);

      if (result == 2) {
        await Pedidos().getPedidos(
            LoginData.user.idAdmin.toString(),
            LoginData.user.idCrm.toString(),
            LoginData.user.idPerfil.toString());
        progressDialog.dismiss();
        setState(() {});
        showDialog(
            context: context,
            builder: (context) {
              return AlertDialog(
                content: Text("Pedido cancelado correctamente"),
                actions: [
                  TextButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: Text("Ok"))
                ],
              );
            });
      } else {
        progressDialog.dismiss();
        showDialog(
            context: context,
            builder: (context) {
              return AlertDialog(
                content: Text("Ocurrio un error al cancelar el pedido"),
              );
            });
      }
    } else {
      SharedPreferences shared = await SharedPreferences.getInstance();
      if (shared.getStringList("pendingCancel") == null) {
        shared.setStringList('pendingCancel', [jsonEncode(cancelar)]);
      } else {
        List<String> pending =
        shared.getStringList("pendingCancel") as List<String>;
        pending.add(jsonEncode(cancelar));
        shared.setStringList('pendingCancel', pending);
      }
      showDialog(
          context: context,
          builder: (context) {
            return AlertDialog(
              content: Text(
                  "El pedido se cancelará cuando cuente con conexión a internet"),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text("Ok"))
              ],
            );
          });
    }
  }

  void showDetalle(int index){
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            content: SingleChildScrollView(
              child: Column(
                children: [
                  Column(
                      children:
                      Detalle.detalle.data!.map((e) {
                        int index2 =
                        Detalle.detalle.data!.indexOf(e);
                        List<Seleccionado>? select =
                            Detalle.detalle.seleccionados;

                        bool error = false;
                        try {
                          if(Provider.of<Session>(context,
                              listen: false)
                              .cantidad.length != Detalle.detalle.data!.length){
                            Provider.of<Session>(context,
                                listen: false)
                                .addPrecios(double.parse(select!
                                .elementAt(index2)
                                .precio
                                .toString()));
                            Provider.of<Session>(context,
                                listen: false)
                                .addCantidad(int.parse(select!
                                .elementAt(index2)
                                .cantidad
                                .toString()));
                          }

                          int.parse(select!
                              .elementAt(index2)
                              .cantidad
                              .toString());
                          error = false;
                        } catch (_) {
                          if(Provider.of<Session>(context,listen: false).cantidad.length != Detalle.detalle.data!.length){
                            Provider.of<Session>(context,
                                listen: false)
                                .addCantidad(0);
                            Provider.of<Session>(context,
                                listen: false)
                                .addPrecios(double.parse(e.precio.toString()));
                          }
                          error = true;
                        }

                        return ListTile(
                          title: Text(
                            e.nombreDelProducto.toString(),
                            style: TextStyle(
                                fontWeight: FontWeight.bold),
                          ),
                          subtitle: Align(
                            alignment: Alignment.centerLeft,
                            child: Padding(
                              padding: const EdgeInsets.only(
                                  left: 10.0),
                              child: Column(
                                crossAxisAlignment:
                                CrossAxisAlignment.start,
                                children: [
                                  Text("Precio: " + "\$" +
                                      (!error ?select!
                                          .elementAt(
                                          index2)
                                          .precio
                                          .toString() : e.precio.toString())),
                                  Row(
                                    mainAxisAlignment:
                                    MainAxisAlignment.start,
                                    mainAxisSize:
                                    MainAxisSize.min,
                                    children: [
                                      Text("Cantidad: "),
                                      SizedBox(
                                        width:
                                        MediaQuery.of(context)
                                            .size
                                            .width *
                                            0.2,
                                        height: 30,
                                        child: TextFormField(
                                          initialValue: Provider.of<Session>(
                                              context,
                                              listen:
                                              false)
                                              .cantidad[
                                          index2]
                                              .toString() == "0" ? "" : Provider.of<Session>(
                                              context,
                                              listen:
                                              false)
                                              .cantidad[
                                          index2]
                                              .toString(),
                                          keyboardType:
                                          TextInputType
                                              .number,
                                          inputFormatters: [
                                            FilteringTextInputFormatter
                                                .allow(RegExp(
                                                "[0-9]"))
                                          ],
                                          onChanged: (c) {
                                            if (c.isNotEmpty) {
                                              Provider.of<Session>(
                                                  context,
                                                  listen:
                                                  false)
                                                  .editCantidad(
                                                  index2,
                                                  int.parse(
                                                      c));

                                            }else{
                                              Provider.of<Session>(
                                                  context,
                                                  listen:
                                                  false)
                                                  .editCantidad(
                                                  index2,
                                                  0);
                                            }
                                          },
                                          decoration:
                                          InputDecoration(
                                              border:
                                              OutlineInputBorder(
                                                borderRadius:
                                                BorderRadius
                                                    .circular(8),
                                              )),
                                        ),
                                      ),
                                    ],
                                  ),
                                  Consumer<Session>(
                                      builder: (context, _, __) => Text("Total: " +
                                          "\$" +
                                          ((double.parse(Provider.of<Session>(
                                              context,
                                              listen:
                                              false)
                                              .precios[
                                          index2]
                                              .toString()) *
                                              double.parse(Provider.of<Session>(
                                                  context,
                                                  listen:
                                                  false)
                                                  .cantidad[
                                              index2]
                                                  .toString()))
                                              .toString()
                                          ))),
                                ],
                              ),
                            ),
                          ),
                        );
                      }).toList()),
                  const Divider(),
                  Metodos(),
                  const Divider(),

                  Consumer<Session>(
                      builder: (context, _, __) => Text("TOTAL: " + "\$"  + geTotal(Detalle.detalle.seleccionados).toString(), style: TextStyle(fontWeight: FontWeight.bold),)
                  ),
                ],
              ),
            ),
            actions: [
              TextButton(onPressed: (){
                Navigator.of(context).pop();
              }, child: Text("CERRAR")),
              TextButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                    showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return AlertDialog(
                            content: Text(
                                "¿Seguro que quieres cancelar el pedido con nombre de ${Pedidos.pedidos.data!.values.elementAt(index).nombre.toString()}?"),
                            actions: <Widget>[
                              TextButton(
                                child: Text(
                                  "No",
                                  style: TextStyle(
                                      color: Colors.black),
                                ),
                                onPressed: () {
                                  Navigator.of(context)
                                      .pop(false);
                                },
                              ),
                              TextButton(
                                child: Text(
                                  "Si",
                                  style: TextStyle(
                                      color: Colors.red),
                                ),
                                onPressed: () {
                                  cancelPedidos(
                                      Clientes.clientes.data!.values.elementAt(index)
                                          .idLr ??
                                          "",

                                          "",

                                          "");
                                },
                              ),
                            ],
                          );
                        });
                  },
                  child: Text("CANCELAR")),
              TextButton(
                  onPressed: () {
                    if(geTotal(Detalle.detalle.seleccionados) != 0.0){
                      if(Provider.of<Session>(context,listen: false).payment == "Contado" ){
                        if(Provider.of<Session>(context,listen: false).pagos.isNotEmpty){
                          entregarPedido(
                              Clientes.clientes.data!.values.elementAt(index)
                                  .idLr ??
                                  "",
                              ""
                                  "",
                                  "",  Detalle.detalle.idDominio.toString(),Clientes.clientes.data!.values.elementAt(index).nombre.toString() + (Clientes.clientes.data!.values.elementAt(index).nombreDelNegocio.toString().isNotEmpty || Clientes.clientes.data!.values.elementAt(index).alias.toString().isNotEmpty ? " (" : "") + Clientes.clientes.data!.values.elementAt(index).nombreDelNegocio.toString() + ((Clientes.clientes.data!.values.elementAt(index).alias.toString().isNotEmpty ? ("-> " + Clientes.clientes.data!.values.elementAt(index).alias.toString()) : "") + ((Clientes.clientes.data!.values.elementAt(index).nombreDelNegocio.toString().isNotEmpty || Clientes.clientes.data!.values.elementAt(index).alias.toString().isNotEmpty) ? ")" : "")));
                        }else{
                          ScaffoldMessenger.of(context).showSnackBar(
                              SnackBar(content: Text("Seleccione un metodo de pago")));
                        }
                      }else if(Provider.of<Session>(context,listen: false).payment == "Vales" || Provider.of<Session>(context,listen: false).payment == "Credito"){
                        Provider.of<Session>(context,listen: false).pagos = "";
                        entregarPedido(
                            Clientes.clientes.data!.values.elementAt(index)
                                .idLr ??
                                "",
                                "",
                                "",  Detalle.detalle.idDominio.toString(),Clientes.clientes.data!.values.elementAt(index).nombre.toString() + (Clientes.clientes.data!.values.elementAt(index).nombreDelNegocio.toString().isNotEmpty || Clientes.clientes.data!.values.elementAt(index).alias.toString().isNotEmpty ? " (" : "") + Clientes.clientes.data!.values.elementAt(index).nombreDelNegocio.toString() + ((Clientes.clientes.data!.values.elementAt(index).alias.toString().isNotEmpty ? ("-> " + Clientes.clientes.data!.values.elementAt(index).alias.toString()) : "") + ((Clientes.clientes.data!.values.elementAt(index).nombreDelNegocio.toString().isNotEmpty || Clientes.clientes.data!.values.elementAt(index).alias.toString().isNotEmpty) ? ")" : "")));
                      }else{
                        ScaffoldMessenger.of(context).showSnackBar(
                            SnackBar(content: Text("Seleccione un metodo de pago")));
                      }
                    }else{
                      ScaffoldMessenger.of(context).showSnackBar(
                          SnackBar(content: Text("Seleccione una cantidad")));
                    }

                  }, child: Text("ENTREGAR"))
            ],
          );
        });
  }

  double geTotal(List<Seleccionado>? select){
    double tot = 0.0;
    int index = 0;
    Provider.of<Session>(
        context,
        listen:
        false)
        .precios.forEach((element) {
      tot = tot + ((double.parse(element.toString()) * Provider.of<Session>(
          context,
          listen:
          false)
          .cantidad[index]));
      index = index +1 ;
    });
    return tot;
  }
  Future<Position?> infoPermission()async{
    LocationPermission permission;
    permission = await Geolocator.checkPermission();
    if(permission == LocationPermission.denied) {
      return await showDialog(context: context, builder: (context) {
        return AlertDialog(
          title: Text("Por favor otorgue accesso a su ubicación"),
          content: Text(
              "Utilizamos la ubicación para poder registrar de mejor manera los datos de dirección de los clientes, ademas de poder realizar tracking correcto de la ruta de los repartidores de los pedidos"),
          actions: [
            TextButton(onPressed: () async{
              Navigator.of(context).pop(await _determinePosition());
            }, child: Text("Ok"))
          ],
        );
      });
    }else{
      return _determinePosition();
    }
  }

  Future<Position> _determinePosition() async {
    bool serviceEnabled;
    LocationPermission permission;

    // Test if location services are enabled.
    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      showDialog(context: context, builder: (context) {
        return AlertDialog(
          title: Text("Por favor active el GPS de su dispositivo"),
          content: Text(
              "Utilizamos la ubicación para poder registrar de mejor manera los datos de dirección de los clientes"),
          actions: [
            TextButton(onPressed: () {
              Geolocator.openLocationSettings();
              Navigator.of(context).pop();
            }, child: Text("Ok"))
          ],
        );
      });
    }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        showDialog(context: context, builder: (context) {
          return AlertDialog(
            title: const Text("Por favor otorgue accesso a su ubicación en la configuración del sistema"),
            content: Text(
                "Utilizamos la ubicación para poder registrar de mejor manera los datos de dirección de los clientes"),
            actions: [
              TextButton(onPressed: () {
                Geolocator.openAppSettings();
                Navigator.of(context).pop();
              }, child: Text("Ok"))
            ],
          );
        });
      }
    }

    if (permission == LocationPermission.deniedForever) {
      // Permissions are denied forever, handle appropriately.
      showDialog(context: context, builder: (context) {
        return AlertDialog(
          title: Text("Por favor otorgue accesso a su ubicación en la configuración del sistema"),
          content: Text(
              "Utilizamos la ubicación para poder registrar de mejor manera los datos de dirección de los clientes"),
          actions: [
            TextButton(onPressed: () {
              Geolocator.openLocationSettings();
              Navigator.of(context).pop();
            }, child: Text("Ok"))
          ],
        );
      });
    }
    return await Geolocator.getCurrentPosition();
  }
}
