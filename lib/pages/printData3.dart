// ignore_for_file: public_member_api_docs

import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:ippsonline/models/totales.dart';
import 'package:pdf/pdf.dart';
import 'package:pdf/widgets.dart' as pw;
import 'package:printing/printing.dart';

import '../models/detalleVenta.dart';
import '../models/direccion.dart';
import '../models/ventaModel.dart';


class PrintData3 extends StatelessWidget {
  List<List<DetalleVenta>> venta;
  String name;
  Direccion direccion;
  PrintData3(this.venta, this.name,this.direccion,{Key? key}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: PdfPreview(
          build: (format) => _generatePdf(format, venta,name,context),
        ),
      ),
    );
  }

  Future<Uint8List> _generatePdf(PdfPageFormat format, List<List<DetalleVenta>> venta, index, a) async {
    final pdf = pw.Document(version: PdfVersion.pdf_1_5, compress: true,);
    final font = await PdfGoogleFonts.nunitoExtraLight();
    var image = await imageFromAssetBundle("assets/logo/logo.jpg");
    List<pw.Widget> widgets = [];
    widgets.add(
      pw.SizedBox(
          width: double.infinity,
          child: pw.Column(
              crossAxisAlignment: pw.CrossAxisAlignment.center,
              children: [

                pw.Text(direccion.data!.titulo.toString(), style: pw.TextStyle(fontSize: 18)),
                pw.Text(direccion.data!.subTitulo.toString(), style: pw.TextStyle(fontSize: 15)),
                pw.SizedBox(height: 10),
                pw.Text(direccion.data!.header.toString().toString(), style: pw.TextStyle(fontSize: 10)),
                pw.SizedBox(height: 20),
              ]
          )
      ),
    );
    widgets.add(

      pw.Column(
          children: Venta.ventas.map((e) {
            return pw.Column(
                children: [
                  pw.SizedBox(
                    width: double.infinity,
                    child: pw.Row(
                        children: [

                          pw.Text(e.nombre.toString() + (e.nombreDelNegocio.toString().isNotEmpty || e.alias.toString().isNotEmpty ? " (" : "") + e.nombreDelNegocio.toString() + ((e.alias.toString().isNotEmpty ? ("-> " + e.alias.toString()) : "") + ((e.nombreDelNegocio.toString().isNotEmpty || e.alias.toString().isNotEmpty) ? ")" : "")), style: pw.TextStyle(fontSize: 14)),
                        ]
                    ),
                  ),
                  pw.SizedBox(height: 10),
                  pw.Table(
                      columnWidths: <int, pw.TableColumnWidth>{
                        0: pw.FlexColumnWidth(4),
                        1: pw.FlexColumnWidth(1.5),
                        2: pw.FlexColumnWidth(1.5),
                        3: pw.FlexColumnWidth(2),
                      },
                      defaultVerticalAlignment: pw.TableCellVerticalAlignment.middle,
                      children: [
                        pw.TableRow(
                            children: [
                              pw.Text("PRODUCTO", style: pw.TextStyle(fontWeight: pw.FontWeight.bold)),
                              pw.Text("PRECIO", style: pw.TextStyle(fontWeight: pw.FontWeight.bold)),
                              pw.Text("CANTIDAD", style: pw.TextStyle(fontWeight: pw.FontWeight.bold)),
                              pw.Text("TOTAL", style: pw.TextStyle(fontWeight: pw.FontWeight.bold)),
                            ]
                        )
                      ]
                  ),
                  pw.SizedBox(height: 10),
                  pw.Table(
                    columnWidths: <int, pw.TableColumnWidth>{
                      0: pw.FlexColumnWidth(4),
                      1: pw.FlexColumnWidth(1.5),
                      2: pw.FlexColumnWidth(1.5),
                      3: pw.FlexColumnWidth(2),
                    },
                    defaultVerticalAlignment: pw.TableCellVerticalAlignment.middle,
                    children: venta.elementAt(Venta.ventas.indexOf(e)).map((a){
                      return pw.TableRow(
                          children: [
                            pw.Text(a.nombreDelProducto.toString(), style: pw.TextStyle(fontSize: 12)),
                            pw.Text("\$" + a.precioVendido.toString()),
                            pw.Text(a.cantidad.toString()),
                            pw.Text("\$" +a.total.toString()),
                          ]
                      );
                    }).toList(),
                  ),
                  pw.Divider(thickness: 0.01),
                  pw.Table(
                      columnWidths: <int, pw.TableColumnWidth>{
                        0: pw.FlexColumnWidth(4),
                        1: pw.FlexColumnWidth(1.5),
                        2: pw.FlexColumnWidth(1.5),
                        3: pw.FlexColumnWidth(2),
                      },
                      defaultVerticalAlignment: pw.TableCellVerticalAlignment.middle,
                      children: [
                        pw.TableRow(
                            children: [
                              pw.Text("", style: pw.TextStyle(fontSize: 12)),
                              pw.Text(""),
                              pw.Text(""),
                              pw.Text("\$" +getTotal(venta.elementAt(Venta.ventas.indexOf(e)))),
                            ]
                        )
                      ]
                  ),
                  pw.SizedBox(height: 15)
                ]
            );
          }).toList()
      )
    );
    widgets.add(
      pw.Divider(thickness: 0.01)
    );
    widgets.add(
      pw.Table(
          columnWidths: <int, pw.TableColumnWidth>{
            0: pw.FlexColumnWidth(4),
            1: pw.FlexColumnWidth(1.5),
            2: pw.FlexColumnWidth(1.5),
            3: pw.FlexColumnWidth(2),
          },
          defaultVerticalAlignment: pw.TableCellVerticalAlignment.middle,
          children: [
            pw.TableRow(
                children: [
                  pw.Text("", style: pw.TextStyle(fontSize: 12)),
                  pw.Text(""),
                  pw.Text(""),
                  pw.Text("\$" +getTotalFull(venta)),
                ]
            )
          ]
      ),
    );
    widgets.add(
      pw.SizedBox(height: 20),
    );
    widgets.add(
      pw.Table(
          columnWidths: <int, pw.TableColumnWidth>{
            0: pw.FlexColumnWidth(3),
            1: pw.FlexColumnWidth(1),
            2: pw.FlexColumnWidth(1),
          },
          defaultVerticalAlignment: pw.TableCellVerticalAlignment.middle,
          children: [
            pw.TableRow(
                children: [
                  pw.Text("Total garrafones en efectivo",style: pw.TextStyle(fontWeight: pw.FontWeight.bold),textAlign: pw.TextAlign.right,),
                  pw.Text(Totales.totales.data!.totalGarrafonesEnEfectivo!.elementAt(0).toString(),textAlign: pw.TextAlign.center),
                  pw.Text(Totales.totales.data!.totalGarrafonesEnEfectivo!.elementAt(1).toString(),textAlign: pw.TextAlign.center),
                ]
            ),
            pw.TableRow(
                children: [
                  pw.Text("Total garrafones en Vales",style: pw.TextStyle(fontWeight: pw.FontWeight.bold),textAlign: pw.TextAlign.right,),
                  pw.Text(Totales.totales.data!.totalGarrafonesEnVales!.elementAt(0).toString(),textAlign: pw.TextAlign.center),
                  pw.Text(Totales.totales.data!.totalGarrafonesEnVales!.elementAt(1).toString(),textAlign: pw.TextAlign.center)
                ]
            ),
            pw.TableRow(
                children: [
                  pw.Text("Total garrafones a Crédito",style: pw.TextStyle(fontWeight: pw.FontWeight.bold),textAlign: pw.TextAlign.right,),
                  pw.Text(Totales.totales.data!.totalGarrafonesACrdito!.elementAt(0).toString(),textAlign: pw.TextAlign.center),
                  pw.Text(Totales.totales.data!.totalGarrafonesACrdito!.elementAt(1).toString(),textAlign: pw.TextAlign.center)
                ]
            ),
            pw.TableRow(
                children: [
                  pw.Text("Total General",style: pw.TextStyle(fontWeight: pw.FontWeight.bold),textAlign: pw.TextAlign.right,),
                  pw.Text(Totales.totales.data!.totalGeneral!.elementAt(0).toString(),textAlign: pw.TextAlign.center),
                  pw.Text(Totales.totales.data!.totalGeneral!.elementAt(1).toString(),textAlign: pw.TextAlign.center),
                ]
            ),
            pw.TableRow(
                children: [
                  pw.Text("Stock de garrafones",style: pw.TextStyle(fontWeight: pw.FontWeight.bold),textAlign: pw.TextAlign.right,),
                  pw.Text(Totales.totales.data!.stockDeGarrafones!.elementAt(0).toString(),textAlign: pw.TextAlign.center),
                  pw.Text(Totales.totales.data!.stockDeGarrafones!.elementAt(1).toString(),textAlign: pw.TextAlign.center,),
                ]
            ),
          ]
      ),
    );
    widgets.add(pw.Spacer());
    widgets.add(
      pw.Align(
        alignment: pw.Alignment.center,
        child:  pw.Text(direccion.data!.footer.toString(), style: pw.TextStyle(fontSize: 10), textAlign: pw.TextAlign.center)
      )
    );
    /*widgets.add(
        pw.Column(
          crossAxisAlignment: pw.CrossAxisAlignment.start,
          children: [
            pw.SizedBox(
                width: double.infinity,
                child: pw.Column(
                    crossAxisAlignment: pw.CrossAxisAlignment.center,
                    children: [

                      pw.Text(direccion.data!.titulo.toString(), style: pw.TextStyle(fontSize: 18)),
                      pw.Text(direccion.data!.subTitulo.toString(), style: pw.TextStyle(fontSize: 15)),
                      pw.SizedBox(height: 10),
                      pw.Text(direccion.data!.header.toString().toString(), style: pw.TextStyle(fontSize: 10)),
                      pw.SizedBox(height: 20),
                    ]
                )
            ),

            pw.Column(
                children: Venta.ventas.map((e) {
                  return pw.Column(
                      children: [
                        pw.SizedBox(
                          width: double.infinity,
                          child: pw.Row(
                              children: [

                                pw.Text(e.nombre.toString() + (e.nombreDelNegocio.toString().isNotEmpty || e.alias.toString().isNotEmpty ? " (" : "") + e.nombreDelNegocio.toString() + ((e.alias.toString().isNotEmpty ? ("-> " + e.alias.toString()) : "") + ((e.nombreDelNegocio.toString().isNotEmpty || e.alias.toString().isNotEmpty) ? ")" : "")), style: pw.TextStyle(fontSize: 14)),
                              ]
                          ),
                        ),
                        pw.SizedBox(height: 10),
                        pw.Table(
                            columnWidths: <int, pw.TableColumnWidth>{
                              0: pw.FlexColumnWidth(4),
                              1: pw.FlexColumnWidth(1.5),
                              2: pw.FlexColumnWidth(1.5),
                              3: pw.FlexColumnWidth(2),
                            },
                            defaultVerticalAlignment: pw.TableCellVerticalAlignment.middle,
                            children: [
                              pw.TableRow(
                                  children: [
                                    pw.Text("PRODUCTO", style: pw.TextStyle(fontWeight: pw.FontWeight.bold)),
                                    pw.Text("PRECIO", style: pw.TextStyle(fontWeight: pw.FontWeight.bold)),
                                    pw.Text("CANTIDAD", style: pw.TextStyle(fontWeight: pw.FontWeight.bold)),
                                    pw.Text("TOTAL", style: pw.TextStyle(fontWeight: pw.FontWeight.bold)),
                                  ]
                              )
                            ]
                        ),
                        pw.SizedBox(height: 10),
                        pw.Table(
                          columnWidths: <int, pw.TableColumnWidth>{
                            0: pw.FlexColumnWidth(4),
                            1: pw.FlexColumnWidth(1.5),
                            2: pw.FlexColumnWidth(1.5),
                            3: pw.FlexColumnWidth(2),
                          },
                          defaultVerticalAlignment: pw.TableCellVerticalAlignment.middle,
                          children: venta.elementAt(Venta.ventas.indexOf(e)).map((a){
                            return pw.TableRow(
                                children: [
                                  pw.Text(a.nombreDelProducto.toString(), style: pw.TextStyle(fontSize: 12)),
                                  pw.Text("\$" + a.precioVendido.toString()),
                                  pw.Text(a.cantidad.toString()),
                                  pw.Text("\$" +a.total.toString()),
                                ]
                            );
                          }).toList(),
                        ),
                        pw.Divider(thickness: 0.01),
                        pw.Table(
                            columnWidths: <int, pw.TableColumnWidth>{
                              0: pw.FlexColumnWidth(4),
                              1: pw.FlexColumnWidth(1.5),
                              2: pw.FlexColumnWidth(1.5),
                              3: pw.FlexColumnWidth(2),
                            },
                            defaultVerticalAlignment: pw.TableCellVerticalAlignment.middle,
                            children: [
                              pw.TableRow(
                                  children: [
                                    pw.Text("", style: pw.TextStyle(fontSize: 12)),
                                    pw.Text(""),
                                    pw.Text(""),
                                    pw.Text("\$" +getTotal(venta.elementAt(Venta.ventas.indexOf(e)))),
                                  ]
                              )
                            ]
                        ),
                        pw.SizedBox(height: 15)
                      ]
                  );
                }).toList()
            ),
            pw.Divider(thickness: 0.01),
            pw.Table(
                columnWidths: <int, pw.TableColumnWidth>{
                  0: pw.FlexColumnWidth(4),
                  1: pw.FlexColumnWidth(1.5),
                  2: pw.FlexColumnWidth(1.5),
                  3: pw.FlexColumnWidth(2),
                },
                defaultVerticalAlignment: pw.TableCellVerticalAlignment.middle,
                children: [
                  pw.TableRow(
                      children: [
                        pw.Text("", style: pw.TextStyle(fontSize: 12)),
                        pw.Text(""),
                        pw.Text(""),
                        pw.Text("\$" +getTotalFull(venta)),
                      ]
                  )
                ]
            ),


            *//* pw.Divider(thickness: 0.01),
              pw.Table(
                  columnWidths: <int, pw.TableColumnWidth>{
                    0: pw.FlexColumnWidth(4),
                    1: pw.FlexColumnWidth(1.5),
                    2: pw.FlexColumnWidth(1.5),
                    3: pw.FlexColumnWidth(2),
                  },
                  defaultVerticalAlignment: pw.TableCellVerticalAlignment.middle,
                  children: [
                    pw.TableRow(
                        children: [
                          pw.Text("", style: pw.TextStyle(fontSize: 12)),
                          pw.Text(""),
                          pw.Text(""),
                          pw.Text("\$" +getTotal(venta)),
                        ]
                    )
                  ]
              ),*//*
            pw.Spacer(),
            pw.Expanded(
              child: pw.Text(direccion.data!.footer.toString(), style: pw.TextStyle(fontSize: 10), textAlign: pw.TextAlign.center),
            ),
          ],
        )
    );*/
    pdf.addPage(
      pw.MultiPage(
        margin: const pw.EdgeInsets.only(left: 30, right: 30,top: 15,bottom: 15),
        pageFormat: format,
        build: (context) => widgets
      ),
    );

    return pdf.save();
  }

  String getTotal(List<DetalleVenta> e){
    double a = 0.0;
    e.forEach((element) {
      a = a + double.parse(element.total.toString());
    });
    return a.toString();
  }

  String getTotalFull(List<List<DetalleVenta>> e){
    double a = 0.0;
    e.forEach((element) {
      a = a + double.parse(getTotal(element));
    });
    return a.toString();
  }
}