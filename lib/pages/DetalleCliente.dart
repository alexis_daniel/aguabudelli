import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:ippsonline/pages/printData2.dart';
import 'package:ippsonline/pages/ventasBuilder.dart';
import 'package:ndialog/ndialog.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../data/login.dart';
import '../models/detalleCliente.dart';
import '../models/detallePedido.dart';
import '../models/detalleVenta.dart';
import '../models/direccion.dart';
import '../models/loginModel.dart';
import '../models/pedidosModel.dart';
import '../providers/session.dart';
import '../utils/parameters.dart';
import 'HomePage.dart';

class DetalleCliente2 extends StatefulWidget {
  final DetalleCliente cliente;
  const DetalleCliente2(this.cliente,{super.key});

  @override
  State<DetalleCliente2> createState() => _DetalleCliente2State();
}

class _DetalleCliente2State extends State<DetalleCliente2> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        shape: const RoundedRectangleBorder(borderRadius: BorderRadius.only(bottomRight: Radius.circular(12), bottomLeft: Radius.circular(12))),
        iconTheme: const IconThemeData(color: Colors.white),
        title: const Text("Detalle Cliente", style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white),),
        flexibleSpace: Container(
          decoration:
          const BoxDecoration(
              image: DecorationImage(
                image: AssetImage("assets/logo/background.jpg"),
                fit: BoxFit.cover,
              ),
              borderRadius: BorderRadius.only(bottomRight: Radius.circular(12), bottomLeft: Radius.circular(12))
          ),
        ),
      ),
          body: SingleChildScrollView(
            child: Column(
              children: [
                ListTile(
                  leading: const Icon(Icons.person),
                  title: Text(widget.cliente.datosDelCliente!.nombre.toString()),
                subtitle: Text(widget.cliente.datosDelCliente!.telfono.toString()),
                ),
                const SizedBox(height: 20,),
                Table(
                    columnWidths: <int, TableColumnWidth>{
                      0: FlexColumnWidth(3),
                      1: FlexColumnWidth(1),
                      2: FlexColumnWidth(1),
                    },
                    defaultVerticalAlignment: TableCellVerticalAlignment.middle,
                    children: [
                      TableRow(
                          children: [
                            Text("Total de garrafones en Efectivo",style: TextStyle(fontWeight: FontWeight.w600),textAlign: TextAlign.right,),
                            Text(widget.cliente.detalleDelCliente!.totalGarrafonesEnEfectivo!.elementAt(0).toString(),textAlign: TextAlign.center),
                            Text(widget.cliente.detalleDelCliente!.totalGarrafonesEnEfectivo!.elementAt(1).toString(),textAlign: TextAlign.center),
                          ]
                      ),
                      TableRow(
                          children: [
                            Text("Total garrafones en Vales",style: TextStyle(fontWeight: FontWeight.w600),textAlign: TextAlign.right,),
                            Text(widget.cliente.detalleDelCliente!.totalGarrafonesEnVales!.elementAt(0).toString(),textAlign: TextAlign.center),
                            Text(widget.cliente.detalleDelCliente!.totalGarrafonesEnVales!.elementAt(1).toString(),textAlign: TextAlign.center)
                          ]
                      ),
                      TableRow(
                          children: [
                            Text("Total garrafones a Crédito",style: TextStyle(fontWeight: FontWeight.w600),textAlign: TextAlign.right,),
                            Text(widget.cliente.detalleDelCliente!.totalGarrafonesACrdito!.elementAt(0).toString(),textAlign: TextAlign.center),
                            Text(widget.cliente.detalleDelCliente!.totalGarrafonesACrdito!.elementAt(1).toString(),textAlign: TextAlign.center)
                          ]
                      ),
                      TableRow(
                          children: [
                            Text("Total General",style: TextStyle(fontWeight: FontWeight.w600),textAlign: TextAlign.right,),
                            Text(widget.cliente.detalleDelCliente!.totalGeneral!.elementAt(0).toString(),textAlign: TextAlign.center),
                            Text(widget.cliente.detalleDelCliente!.totalGeneral!.elementAt(1).toString(),textAlign: TextAlign.center),
                          ]
                      ),
                      TableRow(
                          children: [
                            Text("Stock de garrafones",style: TextStyle(fontWeight: FontWeight.w600),textAlign: TextAlign.right,),
                            Text(widget.cliente.detalleDelCliente!.stockDeGarrafones!.elementAt(0).toString(),textAlign: TextAlign.center),
                            Text(widget.cliente.detalleDelCliente!.stockDeGarrafones!.elementAt(1).toString(),textAlign: TextAlign.center,),
                          ]
                      ),

                    ]
                ),
                const SizedBox(height: 20,),
                ListView.builder(
                  shrinkWrap: true,
                  itemBuilder: (context, index){
                return ListTile(
                  title: Text(widget.cliente.pedido!.elementAt(index).nombreDelProducto.toString()),
                  trailing: Text("\$" + widget.cliente.pedido!.elementAt(index).precio.toString()),
                );
                },itemCount: widget.cliente.pedido == null ? 0 : widget.cliente.pedido!.length,),
              ],
            ),
          ),
      floatingActionButton: widget.cliente.pedido != null ? FloatingActionButton.extended(onPressed: (){
        showDetails2(widget.cliente.idLr.toString());
      }, label: const Text("Realizar venta")) : const SizedBox(),
    );
  }


  showDetails2(String index)async{
    var json = await LoginDataRequest().getDetalle(
        LoginData.user.idAdmin.toString(),
        LoginData.user.idCrm.toString(),
        LoginData.user.idPerfil.toString(),
        "",
        index.toString(),
            "");
    Detalle.detalle = Detalle();
    Detalle.detalle = Detalle.fromJson(json);
    showDetalle(0);
  }

  void showDetalle(int index){
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            content: SingleChildScrollView(
              child: Column(
                children: [
                  Column(
                      children:
                      Detalle.detalle.data!.map((e) {
                        int index2 =
                        Detalle.detalle.data!.indexOf(e);
                        List<Seleccionado>? select =
                            Detalle.detalle.seleccionados;

                        bool error = false;
                        try {
                          if(Provider.of<Session>(context,
                              listen: false)
                              .cantidad.length != Detalle.detalle.data!.length){
                            Provider.of<Session>(context,
                                listen: false)
                                .addPrecios(double.parse(select!
                                .elementAt(index2)
                                .precio
                                .toString()));
                            Provider.of<Session>(context,
                                listen: false)
                                .addCantidad(int.parse(select!
                                .elementAt(index2)
                                .cantidad
                                .toString()));
                          }

                          int.parse(select!
                              .elementAt(index2)
                              .cantidad
                              .toString());
                          error = false;
                        } catch (_) {
                          if(Provider.of<Session>(context,listen: false).cantidad.length != Detalle.detalle.data!.length){
                            Provider.of<Session>(context,
                                listen: false)
                                .addCantidad(0);
                            Provider.of<Session>(context,
                                listen: false)
                                .addPrecios(double.parse(e.precio.toString()));
                          }
                          error = true;
                        }

                        return ListTile(
                          title: Text(
                            e.nombreDelProducto.toString(),
                            style: TextStyle(
                                fontWeight: FontWeight.bold),
                          ),
                          subtitle: Align(
                            alignment: Alignment.centerLeft,
                            child: Padding(
                              padding: const EdgeInsets.only(
                                  left: 10.0),
                              child: Column(
                                crossAxisAlignment:
                                CrossAxisAlignment.start,
                                children: [
                                  Text("Precio: " + "\$" +
                                      (!error ?select!
                                          .elementAt(
                                          index2)
                                          .precio
                                          .toString() : e.precio.toString())),
                                  Row(
                                    mainAxisAlignment:
                                    MainAxisAlignment.start,
                                    mainAxisSize:
                                    MainAxisSize.min,
                                    children: [
                                      Text("Cantidad: "),
                                      SizedBox(
                                        width:
                                        MediaQuery.of(context)
                                            .size
                                            .width *
                                            0.2,
                                        height: 30,
                                        child: TextFormField(
                                          initialValue: Provider.of<Session>(
                                              context,
                                              listen:
                                              false)
                                              .cantidad[
                                          index2]
                                              .toString() == "0" ? "" : Provider.of<Session>(
                                              context,
                                              listen:
                                              false)
                                              .cantidad[
                                          index2]
                                              .toString(),
                                          keyboardType:
                                          TextInputType
                                              .number,
                                          inputFormatters: [
                                            FilteringTextInputFormatter
                                                .allow(RegExp(
                                                "[0-9]"))
                                          ],
                                          onChanged: (c) {
                                            if (c.isNotEmpty) {
                                              Provider.of<Session>(
                                                  context,
                                                  listen:
                                                  false)
                                                  .editCantidad(
                                                  index2,
                                                  int.parse(
                                                      c));

                                            }else{
                                              Provider.of<Session>(
                                                  context,
                                                  listen:
                                                  false)
                                                  .editCantidad(
                                                  index2,
                                                  0);
                                            }
                                          },
                                          decoration:
                                          InputDecoration(
                                              border:
                                              OutlineInputBorder(
                                                borderRadius:
                                                BorderRadius
                                                    .circular(8),
                                              )),
                                        ),
                                      ),
                                    ],
                                  ),
                                  Consumer<Session>(
                                      builder: (context, _, __) => Text("Total: " +
                                          "\$" +
                                          ((double.parse(Provider.of<Session>(
                                              context,
                                              listen:
                                              false)
                                              .precios[
                                          index2]
                                              .toString()) *
                                              double.parse(Provider.of<Session>(
                                                  context,
                                                  listen:
                                                  false)
                                                  .cantidad[
                                              index2]
                                                  .toString()))
                                              .toString()
                                          ))),
                                ],
                              ),
                            ),
                          ),
                        );
                      }).toList()),
                  const Divider(),
                  Metodos(),
                  const Divider(),

                  Consumer<Session>(
                      builder: (context, _, __) => Text("TOTAL: " + "\$"  + geTotal(Detalle.detalle.seleccionados).toString(), style: TextStyle(fontWeight: FontWeight.bold),)
                  ),
                ],
              ),
            ),
            actions: [
              TextButton(onPressed: (){
                Navigator.of(context).pop();
              }, child: Text("CERRAR")),
              TextButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                    showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return AlertDialog(
                            content: Text(
                                "¿Seguro que quieres cancelar el pedido?"),
                            actions: <Widget>[
                              TextButton(
                                child: Text(
                                  "No",
                                  style: TextStyle(
                                      color: Colors.black),
                                ),
                                onPressed: () {
                                  Navigator.of(context)
                                      .pop(false);
                                },
                              ),
                              TextButton(
                                child: Text(
                                  "Si",
                                  style: TextStyle(
                                      color: Colors.red),
                                ),
                                onPressed: () {
                                  cancelPedidos(
                                      widget.cliente.idLr.toString(),

                                          "",

                                          "");
                                },
                              ),
                            ],
                          );
                        });
                  },
                  child: Text("CANCELAR")),
              TextButton(
                  onPressed: () {
                    if(geTotal(Detalle.detalle.seleccionados) != 0.0){
                      if(Provider.of<Session>(context,listen: false).payment == "Contado" || Provider.of<Session>(context,listen: false).payment == "Credito" ){
                        if(Provider.of<Session>(context,listen: false).pagos.isNotEmpty){
                          entregarPedido(
                              widget.cliente.idLr.toString(),

                                  "",

                                  "",  Detalle.detalle.idDominio.toString(),"");
                        }else{
                          ScaffoldMessenger.of(context).showSnackBar(
                              SnackBar(content: Text("Seleccione un metodo de pago")));
                        }
                      }else if(Provider.of<Session>(context,listen: false).payment == "Vales"){
                        Provider.of<Session>(context,listen: false).pagos = "";
                        entregarPedido(
                    widget.cliente.idLr.toString(),

                                "",

                                "",  Detalle.detalle.idDominio.toString(),"");
                      }else{
                        ScaffoldMessenger.of(context).showSnackBar(
                            SnackBar(content: Text("Seleccione un metodo de pago")));
                      }
                    }else{
                      ScaffoldMessenger.of(context).showSnackBar(
                          SnackBar(content: Text("Seleccione una cantidad")));
                    }

                  }, child: Text("ENTREGAR"))
            ],
          );
        });
  }

  void entregarPedido(String lr, String web, String programado,
      String dominio, String name) async {
    Navigator.of(context).pop(false);
    int cantidad = 0;
    List<int> elementsIndex = [];
    print(Provider.of<Session>(context,listen: false).cantidad);
    Provider.of<Session>(context,listen: false).cantidad.forEach((element) {
      cantidad = cantidad +element;
      if(element != 0 ){
        elementsIndex.add(Provider.of<Session>(context,listen: false).cantidad.indexOf(element));
      }
    });
    Map<String, dynamic> productos= {};
    List<DetalleVenta> venta = [];
    elementsIndex.forEach((element) {
      var a = Detalle.detalle.data!.elementAt(element);
      Map<String, dynamic> data =  {
        a.idProducto.toString() : {
          "Puntos": "",
          "id_producto": a.idProducto.toString(),
          "NombreDelProducto": a.nombreDelProducto,
          "PrecioVendido": Provider.of<Session>(context,listen: false).precios.elementAt(element).toString(),
          "id_EntSal": "",
          "Cantidad": Provider.of<Session>(context,listen: false).cantidad.elementAt(element).toString(),
        }
      };
      venta.add(
          DetalleVenta(
              cantidad: Provider.of<Session>(context,listen: false).cantidad.elementAt(element).toString(),
              subTotal: Provider.of<Session>(context,listen: false).precios.elementAt(element).toString(),
              nombreDelProducto: a.nombreDelProducto,
              precioVendido: Provider.of<Session>(context,listen: false).precios.elementAt(element).toString(),
              total: (double.parse(Provider.of<Session>(context,listen: false).cantidad.elementAt(element).toString()) * double.parse(Provider.of<Session>(context,listen: false).precios.elementAt(element).toString())).toString()
          )
      );
      productos.addAll(data);
    });

    Map entregar = {
      "id_dominio": dominio,
      "id_admin": LoginData.user.idAdmin.toString(),
      "id_CRM": LoginData.user.idCrm.toString(),
      "id_perfil": LoginData.user.idPerfil.toString(),
      "id_LR": lr,
      "id_PWeb": web,
      "id_programado": programado,
      "FolioPersonalizado": "",
      "FechaDeSolicitud": DateFormat('yyyy-MM-dd').format(DateTime.now()),
      "Cantidad":cantidad,
      "SubTotal":geTotal(Detalle.detalle.seleccionados),
      "Total":geTotal(Detalle.detalle.seleccionados),
      "ArrayProductos": productos,
      "TipoDePago": Provider.of<Session>(context,listen: false).payment,
      "MetodoDePago": Provider.of<Session>(context,listen: false).pagos,
      "Referencia": "",
      "Observaciones": "",
    };


    if (await Parameters().checkInternet()) {
      CustomProgressDialog progressDialog = CustomProgressDialog(
        context,
        blur: 10,
      );
      //Set loading with red circular progress indicator
      progressDialog.setLoadingWidget(const CircularProgressIndicator(
          valueColor: AlwaysStoppedAnimation(Colors.red)));
      progressDialog.show();


      int result = await LoginDataRequest().entregarPedido(entregar,context);

      if (result == 2) {
        await Pedidos().getPedidos(
            LoginData.user.idAdmin.toString(),
            LoginData.user.idCrm.toString(),
            LoginData.user.idPerfil.toString());
        progressDialog.dismiss();
        setState(() {});
        showDialog(
            context: context,
            builder: (context) {
              return AlertDialog(
                content: Text("Pedido entregado correctamente"),
                actions: [
                  TextButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: Text("Ok")),
                  TextButton(
                      onPressed: ()async {
                        var json = await LoginDataRequest().getDireccion(LoginData.user.idAdmin.toString(), LoginData.user.idCrm.toString(), Provider.of<Session>(context,listen: false).pv);
                        Navigator.of(context).pop();
                        Navigator.push(context, MaterialPageRoute(builder: (context) => PrintData2(venta,name,Direccion.fromJson(json))));
                      },
                      child: Text("Imprimir")),
                  TextButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                        Navigator.push(context, MaterialPageRoute(builder: (context) => VentasBuilder()));
                      },
                      child: Text("Ir a ventas"))
                ],
              );
            });
      } else {
        progressDialog.dismiss();
        showDialog(
            context: context,
            builder: (context) {
              return AlertDialog(
                content: Text("Ocurrio un error al entregar el pedido"),
              );
            });
      }
    } else {
      SharedPreferences shared = await SharedPreferences.getInstance();
      if (shared.getStringList("pendingEntregar") == null) {
        shared.setStringList('pendingEntregar', [jsonEncode(entregar)]);
      } else {
        List<String> pending =
        shared.getStringList("pendingEntregar") as List<String>;
        pending.add(jsonEncode(entregar));
        shared.setStringList('pendingEntregar', pending);
      }
      showDialog(
          context: context,
          builder: (context) {
            return AlertDialog(
              content: Text(
                  "El pedido se entregará cuando cuente con conexión a internet"),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text("Ok"))
              ],
            );
          });
    }
  }
  double geTotal(List<Seleccionado>? select){
    double tot = 0.0;
    int index = 0;
    Provider.of<Session>(
        context,
        listen:
        false)
        .precios.forEach((element) {
      tot = tot + ((double.parse(element.toString()) * Provider.of<Session>(
          context,
          listen:
          false)
          .cantidad[index]));
      index = index +1 ;
    });
    return tot;
  }

  void cancelPedidos(String lr, String web, String programado) async {
    Navigator.of(context).pop(false);
    Map cancelar = {
      "id_admin": LoginData.user.idAdmin.toString(),
      "id_CRM": LoginData.user.idCrm.toString(),
      "id_perfil": LoginData.user.idAdmin.toString(),
      "id_LR": lr,
      "id_PWeb": web,
      "id_programado": programado
    };
    if (await Parameters().checkInternet()) {
      CustomProgressDialog progressDialog = CustomProgressDialog(
        context,
        blur: 10,
      );
      //Set loading with red circular progress indicator
      progressDialog.setLoadingWidget(const CircularProgressIndicator(
          valueColor: AlwaysStoppedAnimation(Colors.red)));
      progressDialog.show();

      print(cancelar);
      int result = await LoginDataRequest().cancelarPedido(cancelar);

      if (result == 2) {
        await Pedidos().getPedidos(
            LoginData.user.idAdmin.toString(),
            LoginData.user.idCrm.toString(),
            LoginData.user.idPerfil.toString());
        progressDialog.dismiss();
        setState(() {});
        showDialog(
            context: context,
            builder: (context) {
              return AlertDialog(
                content: Text("Pedido cancelado correctamente"),
                actions: [
                  TextButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: Text("Ok"))
                ],
              );
            });
      } else {
        progressDialog.dismiss();
        showDialog(
            context: context,
            builder: (context) {
              return AlertDialog(
                content: Text("Ocurrio un error al cancelar el pedido"),
              );
            });
      }
    } else {
      SharedPreferences shared = await SharedPreferences.getInstance();
      if (shared.getStringList("pendingCancel") == null) {
        shared.setStringList('pendingCancel', [jsonEncode(cancelar)]);
      } else {
        List<String> pending =
        shared.getStringList("pendingCancel") as List<String>;
        pending.add(jsonEncode(cancelar));
        shared.setStringList('pendingCancel', pending);
      }
      showDialog(
          context: context,
          builder: (context) {
            return AlertDialog(
              content: Text(
                  "El pedido se cancelará cuando cuente con conexión a internet"),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text("Ok"))
              ],
            );
          });
    }
  }
}
