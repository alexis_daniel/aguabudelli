import 'package:flutter/material.dart';
import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:ippsonline/models/pedidosModel.dart';
import 'package:provider/provider.dart';
import 'package:http/http.dart' as http;
import '../providers/session.dart';

class MapaPointsAll extends StatefulWidget {
  Pedidos a;
  MapaPointsAll(this.a,{super.key});

  @override
  State<MapaPointsAll> createState() => _MapaPointsAllState();
}

class _MapaPointsAllState extends State<MapaPointsAll> {
  Map<MarkerId, Marker> markers = {};
  late GoogleMapController mapController;

  Map<PolylineId, Polyline> polylines = {};

  List<LatLng> polylineCoordinates = [];

  PolylinePoints polylinePoints = PolylinePoints();

  String googleAPiKey = "AIzaSyD0bDkp8A6MfZrbbn0Tbk2uNNgXCIujB3g";

  @override
  void initState() {
    // TODO: implement initState
    super.initState();


    widget.a.data!.values .forEach((element) async{
      _addMarker(LatLng(double.parse(element.lat.toString()),double.parse(element.lon.toString())), "Text",
          BitmapDescriptor.defaultMarker,InfoWindow(title: element
              .nombre
              .toString() + (element
              .alias.toString().isNotEmpty || element
              .nombreDelNegocio.toString().isNotEmpty ? " (" : "") + (element
              .nombreDelNegocio
              .toString())  +(element
              .alias.toString().isEmpty  ? "" : " -> " + element
              .alias.toString()) +(element
              .alias.toString().isNotEmpty || element
              .nombreDelNegocio.toString().isNotEmpty ? " )" : "")), );

    });
    setState(() {

    });
  }

  @override
  Widget build(BuildContext context) {
    return GoogleMap(
      initialCameraPosition: CameraPosition(target: LatLng(Provider.of<Session>(context,listen: false).lat, Provider.of<Session>(context,listen: false).lng),zoom: 5),
      onMapCreated: _onMapCreated,
      markers: Set<Marker>.of(markers.values),
      polylines: Set<Polyline>.of(polylines.values),
      myLocationEnabled: true,
    );
  }

  void _onMapCreated(GoogleMapController controller) async {
    mapController = controller;
  }

  _addMarker(LatLng position, String id, BitmapDescriptor descriptor, InfoWindow a) {
    MarkerId markerId = MarkerId(id);
    Marker marker =
    Marker(markerId: markerId, icon: descriptor, position: position,infoWindow: a);
    markers[markerId] = marker;
  }
  
  
}
