import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:ippsonline/data/login.dart';
import 'package:ippsonline/pages/DetalleCliente.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';
import 'dart:io';

import '../models/detalleCliente.dart';
import '../models/loginModel.dart';

class QRViewExample extends StatefulWidget {
  const QRViewExample({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _QRViewExampleState();
}

class _QRViewExampleState extends State<QRViewExample> {
  final GlobalKey qrKey = GlobalKey(debugLabel: 'QR');
  Barcode? result;
  QRViewController? controller;

  // In order to get hot reload to work we need to pause the camera if the platform
  // is android, or resume the camera if the platform is iOS.
  @override
  void reassemble() {
    super.reassemble();
    if (Platform.isAndroid) {
      controller!.pauseCamera();
    } else if (Platform.isIOS) {
      controller!.resumeCamera();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: <Widget>[
          Expanded(
            flex: 5,
            child: QRView(
              key: qrKey,
              onQRViewCreated: _onQRViewCreated,
            ),
          ),
          Expanded(
            flex: 1,
            child: Center(
              child: (result != null)
                  ? Text(
                  'QR capturado, por favor espere')
                  : Text('Escanea el QR'),
            ),
          )
        ],
      ),
    );
  }

  void _onQRViewCreated(QRViewController controller) async{
    this.controller = controller;
    controller.scannedDataStream.listen((scanData) async{
      setState(() {
        result = scanData;
      });
      controller.pauseCamera();
      var json = await LoginDataRequest().getDetalleCliente(LoginData.user.idAdmin.toString(),LoginData.user.idCrm.toString(), result!.code.toString());
      DetalleCliente cliente = DetalleCliente.fromJson(json);
      Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => DetalleCliente2(cliente)));
    });
  }

  @override
  void dispose() {
    controller?.dispose();
    super.dispose();
  }
}